from flask import render_template, redirect, url_for, flash, request
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user
from flask_babel import _
from app import db
from app.auth import bp
from app.auth.forms import LoginForm, RegistrationForm, ResetPasswordRequestForm, ResetPasswordForm
from app.models import User
from app.auth.email import send_password_request_email
from flask_babel import lazy_gettext as _l
from app.models import Post


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash(_l('Invalid username or password'))
            return redirect(url_for('auth.login'))
        login_user(user, remember=form.remember_me.data)
        Post.reindex()
        # request.args attribute exposes the contents of the query
        # string in a friendly dictionary format
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)
    return render_template('auth/login.html.j2', title='Sing In', form=form)


@bp.route('/logout')
def logout():
    Post.deleteindex()
    logout_user()
    return redirect(url_for('main.index'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()

    password = form.password.data

    if password:
        is_valid = User.check_password_validity(form.password.data)

        if form.validate_on_submit() and is_valid[0]:
            flash('Validated')
            user = User(username=form.username.data, email=form.email.data)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash(_l('Congrats amigo! You\'re now a registered user!'))
            return redirect(url_for('auth.login'))
        else:
            flash(_l('Check your motherfucking password.'))
            if is_valid[1]['length_error']:
                flash(_l('Check your password length.'))
            if is_valid[1]['digit_error']:
                flash(_l('Password must include at least one digit.'))
            if is_valid[1]['uppercase_error']:
                flash(_l('Password must include at least one uppercase letter.'))
            if is_valid[1]['lowercase_error']:
                flash(_l('Password must include at least one lowercase letter.'))
            if is_valid[1]['symbol_error']:
                flash(_l('Password must include at least one special symbol.'))
            password = ''
    return render_template('auth/register.html.j2', title='Register', form=form)


@bp.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_request_email(user)
        flash(_l('Check your email for the instructions to reset your password.'))
        return redirect(url_for('auth.login'))
    return render_template(
        'auth/reset_password_request.html.j2',
        title=_l('Reset Password'),
        form=form
    )


@bp.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('main.index'))
    form = ResetPasswordForm()
    password = form.password.data
    if password:
        is_valid = User.check_password_validity(form.password.data)
        if form.validate_on_submit() and is_valid[0]:
            user.set_password(form.password.data)
            db.session.commit()
            flash(_l('Your password has been reset!'))
            return redirect(url_for('auth.login'))
        else:
            flash(_l('Check your motherfucking password.'))
            if is_valid[1]['length_error']:
                flash(_l('Check your password length.'))
            if is_valid[1]['digit_error']:
                flash(_l('Password must include at least one digit.'))
            if is_valid[1]['uppercase_error']:
                flash(_l('Password must include at least one uppercase letter.'))
            if is_valid[1]['lowercase_error']:
                flash(_l('Password must include at least one lowercase letter.'))
            if is_valid[1]['symbol_error']:
                flash(_l('Password must include at least one special symbol.'))
            password = ''
    return render_template(
        'auth/reset_password.html.j2',
        form=form
    )
