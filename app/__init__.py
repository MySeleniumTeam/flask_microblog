from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_babel import Babel
from flask_babel import lazy_gettext as _l
from elasticsearch import Elasticsearch
from redis import Redis
import rq
from flask_ckeditor import CKEditor
from flask_wtf.csrf import CSRFProtect
from config import basedir

# Database configuration
db = SQLAlchemy()
migrate = Migrate()
# Mail confoguration
mail = Mail()
# Bootstrap
bootstrap = Bootstrap()
# Login configuration
login = LoginManager()
# Used for page protection
login.login_view = 'auth.login'
login.login_message = _l('Please log in to access this page.')
# Adding JS Moment Library
moment = Moment()
# Adding translation
babel = Babel()
# CKEditor
ckeditor = CKEditor()
# CSRF protection for picture upload
csrf = CSRFProtect()


def create_app(config_class=Config):
    app = Flask(__name__, static_url_path='/static/')
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    mail.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)
    babel.init_app(app)
    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.task_queue = rq.Queue('microblog-tasks', connection=app.redis)
    ckeditor.init_app(app)
    csrf.init_app(app)


    # API
    from app.api import bp as api_bp
    app.register_blueprint(api_bp, urp_prefix='/api')

    # Main Blueprint
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    # Errors Blueprint
    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    # Authentication Blueprint
    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')
    # Logging by email
    # Testing: python -m smtpd -n -c DebuggingServer localhost:8025
    # Gmail:
    # export MAIL_SERVER=smtp.googlemail.com
    # export MAIL_PORT=587
    # export MAIL_USE_TLS=1
    # export MAIL_USERNAME=<your-gmail-username>
    # export MAIL_PASSWORD=<your-gmail-password>
    if not app.debug:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr='noreply@' + app.config['MAIL_SERVER'],
                toaddrs=app.config['ADMINS'],
                subject='Microblog Failure',
                credentials=auth,
                secure=secure
            )
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)
        # File Logging
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/microblog.log', maxBytes=10240 * 4000, backupCount=10)
        file_handler.setFormatter(
            logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('Microblog Startup')

        # Elastic search
        app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
            if app.config['ELASTICSEARCH_URL'] else None


        return app

@babel.localeselector
def get_locale():
    # return request.accept_languages.best_match(app.config['LANGUAGES'])
    return 'ru'


from app import models
