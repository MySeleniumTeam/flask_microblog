from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms import TextAreaField, TextField, SelectField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from wtforms.validators import Length
from app.models import User
from flask_babel import lazy_gettext as _l
from flask_babel import _
from flask import request, flash
from app import db
from flask_ckeditor import CKEditorField


class EditProfileForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    about_me = CKEditorField(_l('About me'), validators=[Length(min=0, max=140)])
    email = StringField(_l('Email Address'), validators=[DataRequired(), Email()])
    submit = SubmitField(_l('Submit'))

    def __init__(self, original_username, original_email, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username
        self.original_email = original_email

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                error_message = _l('Please use a different username.')
                flash(_l(error_message))
                raise ValidationError(_l(error_message))

    def validate_email(self, email):
        if email.data != self.original_email:
            email = User.query.filter_by(email=self.email.data).first()
            if email is not None:
                error_message = _l('Please use a different email address.')
                flash(_l(error_message))
                raise ValidationError(_l(error_message))


class PostForm(FlaskForm):
    title = TextField(
        _l('Header'),
        validators=[DataRequired(), Length(min=1, max=128)]
    )
    post = CKEditorField(
        _l('Say something')
    )
    category = SelectField(_l('Category'))
    submit = SubmitField(_l('Submit'))

class EditPostForm(FlaskForm):
    title = TextField(
        _l('Header'),
        validators=[DataRequired(), Length(min=1, max=128)]
    )
    post = CKEditorField(
        _l('Say something'),
        validators=[DataRequired()]
    )
    submit = SubmitField(_l('Submit'))


class CategoryForm(FlaskForm):
    title = TextField(
        _l('Title'),
        validators=[Length(min=5, max=128)]
    )
    description = CKEditorField(
        _l('Description')
    )
    submit = SubmitField(_l('Submit'))


class EditCategoryForm(FlaskForm):
    title = TextField(
        _l('Title'),
        validators=[Length(min=5, max=128)]
    )
    description = CKEditorField(
        _l('Description')
    )
    submit = SubmitField(_l('Submit'))


class ChangePasswordForm(FlaskForm):
    password = PasswordField(_l('Password'), validators=[DataRequired(), Length(min=12, max=128)])
    password_repeat = PasswordField(
        _l('Repeat Password'),
        validators=[DataRequired(), EqualTo('password'), Length(min=12, max=128)]
    )
    submit = SubmitField(_l('Register'))


class DeletePostApproveForm(FlaskForm):
    choice = BooleanField(
        _l('Are you sure you want to delete the post?')
    )
    submit = SubmitField(_l('Submit'))

class CommentForm(FlaskForm):
    body = TextAreaField(_l('Write here your comment'))
    submit = SubmitField(_l('Submit'))

class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)

class MessageForm(FlaskForm):
    message = CKEditorField(
        _l('Message'),
        validators=[
            Length(min=0, max=140)
        ]
    )
    submit = SubmitField(_l('Submit'))
