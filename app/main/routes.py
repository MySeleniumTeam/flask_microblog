import os
from werkzeug.utils import secure_filename
from flask import render_template, flash, redirect, url_for, request, current_app
from flask import send_from_directory
from app import db
from app.main.forms import EditProfileForm, EditCategoryForm
from flask_login import current_user, login_required
from app.models import User
from datetime import datetime
from app.main.forms import PostForm
from app.models import Post, Rate, Category
from flask_babel import lazy_gettext as _l
from flask import g, make_response
from flask_babel import get_locale
from guess_language import guess_language
from app.main import bp
from app.main.forms import SearchForm
from flask_babel import _
from app.main.forms import MessageForm
from app.models import Message, Notification
from flask import jsonify
from app.main.forms import CommentForm, EditPostForm, DeletePostApproveForm
from app.main.forms import ChangePasswordForm, CategoryForm
from app.models import Comment
from sqlalchemy.sql import func
import random
from flask_ckeditor import upload_success, upload_fail
from flask_sqlalchemy import sqlalchemy

@bp.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.teardown_request
def shutdown_session(exception=None):
    db.session.flush()
    db.session.remove()


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    comments = Comment.query.all()
    username = User.query.filter_by(username=current_user.username).first()
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page,
        current_app.config['POSTS_PER_PAGE'],
        False
    )

    categories = Category.query.all()
    next_url = url_for('main.index', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.index', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template(
        'main/index.html.j2',
        title=_l('The Home Page'),
        posts=posts.items,
        next_url=next_url,
        prev_url=prev_url,
        comments=comments,
        username=username,
        categories=categories
    )


# User page
@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    comments = Comment.query.all()
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page,
        current_app.config['POSTS_PER_PAGE'],
        False
    )
    categories = Category.query.all()
    next_url = url_for('main.user', username=user.username, page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.user', username=user.username, page=posts.prev_num) \
        if posts.has_prev else None

    return render_template(
        'main/user.html.j2',
        user=user,
        posts=posts.items,
        next_url=next_url,
        prev_url=prev_url,
        comments=comments,
        categories=categories
    )


# User profile edit
@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username, current_user.email)
    comments = Comment.query.filter_by(username=current_user.username).all()
    if form.validate_on_submit():
        if comments:
            for comment in comments:
                comment.username = current_user.username
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        current_user.email = form.email.data
        db.session.commit()
        flash(_l('Your changes have been saved'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
        form.email.data = current_user.email
    return render_template('main/edit_profile.html.j2', title='Edit Profile', form=form)


@bp.route('/password/change/<username>', methods=['GET', 'POST'])
@login_required
def change_password(username):
    form = ChangePasswordForm()
    user = User.query.filter_by(username=username).first()
    password = form.password.data
    is_valid = ''
    if password:
        is_valid = User.check_password_validity(password)
        if form.validate_on_submit() and is_valid[0]:
            user.set_password(password)
            db.session.commit()
            flash(_l('Your password has been changed.'))
            return redirect(url_for('main.user', username=current_user.username))
        elif form.password.data != form.password_repeat.data:
            flash(_l('Passwords do not match. Try again'))
        else:
            flash(_l('Check your motherfucking password.'))
            if is_valid[1]['length_error']:
                flash(_l('Check your password length.'))
            if is_valid[1]['digit_error']:
                flash(_l('Password must include at least one digit.'))
            if is_valid[1]['uppercase_error']:
                flash(_l('Password must include at least one uppercase letter.'))
            if is_valid[1]['lowercase_error']:
                flash(_l('Password must include at least one lowercase letter.'))
            if is_valid[1]['symbol_error']:
                flash(_l('Password must include at least one special symbol.'))
            password = ''

    return render_template('main/change_password.html.j2', form=form)


@bp.route('/explore')
@login_required
def explore():
    comments = Comment.query.all()
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page,
        current_app.config['POSTS_PER_PAGE'],
        False
    )
    categories = Category.query.all()
    next_url = url_for('main.explore', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.explore', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template(
        'main/index.html.j2',
        title=_l('Explore'),
        posts=posts.items,
        next_url=next_url,
        prev_url=prev_url,
        comments=comments,
        categories=categories
    )


@bp.route('/search')
@login_required
def search():
    comments = Comment.query.all()
    if not g.search_form.validate():
        return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    users, total = User.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])

    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None

    if users:
        if users.count() < 1:
            users = False
        if posts.count() < 1:
            posts = False
        return render_template(
            'search/search.html.j2',
            title=_('Search'),
            posts=posts,
            next_url=next_url,
            prev_url=prev_url,
            comments=comments,
            users=users
        )
    return render_template(
        'search/search.html.j2',
        title=_('Search'),
        posts=posts,
        next_url=next_url,
        prev_url=prev_url,
        comments=comments
    )


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template(template_name_or_list='main/user_popup.html.j2', user=user)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following {}!'.format(username))
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('main.user', username=username))

@bp.route('/send/message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(
            author=current_user,
            recipient=user,
            body=form.message.data
        )
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash(_('Your message has been sent.'))
        return redirect(url_for('main.user', username=recipient))
    return render_template(
        'messages/send_message.html.j2',
        title=_('Send message'),
        form=form,
        recipient=recipient
    )


@bp.route('/messages')
@login_required
def messages():
    comments = Comment.query.all()
    current_user.last_messages_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()
    ).paginate(
        page,
        current_app.config['POSTS_PER_PAGE'] or 3,
        False
    )
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template(
        'messages/messages.html.j2',
        messages=messages.items,
        next_url=next_url,
        prev_url=prev_url,
        comments=comments
    )

@bp.route('/message/delete/<id>')
@login_required
def delete_message(id):
    message = Message.query.filter_by(id=id).first()
    db.session.delete(message)
    db.session.commit()
    messages = Message.query.filter_by(recipient_id=current_user.id).all()

    return redirect(url_for('main.messages'))



@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since
    ).order_by(
        Notification.timestamp.asc()
    )
    return jsonify([{
        'name': notification.name,
        'data': notification.get_data(),
        'timestamp': notification.timestamp
    } for notification in notifications])


@bp.route('/export_posts')
@login_required
def export_posts():
    if current_user.get_task_in_progress('export_posts'):
        flash(_('An export task is currently in progress'))
    else:
        current_user.launch_task(
            'export_posts',
            _('Exporting posts...')
        )
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))


@bp.route('/send/comment/<int:post_id>', methods=['POST', 'GET'])
@login_required
def send_comment(post_id):
    post = Post.query.get_or_404(post_id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(body=form.body.data, article=post, username=current_user.username)
        db.session.add(comment)
        db.session.commit()
        flash(_l('Your comment was succesfully added.'))
        return redirect(url_for('main.index'))
    return render_template(
        'comments/comment_to_post.html.j2',
        form=form,
        title=_l('Post Comment')
    )


@bp.route('/post/<id>')
@login_required
def show_post(id):
    post = Post.query.filter_by(id=id).first_or_404()
    comments = Comment.query.filter_by(post_id=post.id)
    rates = Rate.query.filter_by(post_id=post.id).all()
    if post.views_amount is None:
        post.views_amount = 1
        db.session.commit()
    else:
        post.views_amount += 1
        db.session.commit()
    total_likes = 0
    total_dislikes = 0
    for rate in rates:
        if rate.like:
            total_likes += 1
        elif rate.dislike:
            total_dislikes += 1

    return render_template(
        'main/single_post.html.j2',
        post=post,
        total_likes=total_likes,
        total_dislikes=total_dislikes,
        comments=comments
    )


@bp.route('/post/create', methods=['GET', 'POST'])
@login_required
def create_post():
    form = PostForm()
    categories = Category.query.all()
    if not categories:
        flash(_l('First Create a Category'))
        return redirect(url_for('main.create_category'))
    elif categories:
        form.category.choices = [(category.title, category.title) for category in categories]
    if form.validate_on_submit():
        language = guess_language(form.post.data)
        post = ''
        if language == 'UNKNOWN' or len(language) > 5:
            language = ''
        category = Category.query.filter_by(title=form.category.data).first()
        post = Post(body=form.post.data, author=current_user, language=language, title=form.title.data, category_id=category.id)
        db.session.add(post)
        db.session.commit()
        flash(_l('Your post is now live!'))
        return redirect(url_for('main.index'))
    return render_template(
        'main/create_post.html.j2',
        title=_l('Creating Post'),
        form=form
    )

# Edit post
@bp.route('/post/edit/<id>', methods=['GET', 'POST'])
@login_required
def edit_post(id):
    post = Post.query.filter_by(id=id).first()
    form = EditPostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.body = form.post.data
        db.session.commit()
        flash(_l('Your changes have been saved'))
        return redirect(url_for('main.show_post', id=id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.post.data = post.body
    return render_template('main/edit_post.html.j2', form=form)

@bp.route('/post/delete/<id>')
@login_required
def delete_post(id):
    post = Post.query.filter_by(id=id).first()
    rate = Rate.query.filter_by(post_id=id).first()
    if rate:
        db.session.delete(rate)
        db.session.commit()
        db.session.delete(post)
        db.session.commit()
    else:
        db.session.delete(post)
        db.session.commit()
    return render_template('main/index.html.j2')

@bp.route('/like/set/<id>', methods=['GET', 'POST'])
@login_required
def set_like(id):
    post = Post.query.filter_by(id=id).first()
    is_rate = Rate.query.filter_by(user_id=current_user.id).filter_by(post_id=post.id).first()
    if not is_rate:
        rate = Rate(user_id=current_user.id, post_id=post.id, like=1, dislike=0)
        db.session.add(rate)
        db.session.commit()
        return redirect(
            url_for('main.show_post', id=id)
        )
    elif is_rate.like == 0 and is_rate.user_id == current_user.id and is_rate.post_id == post.id:
        is_rate.like = 1
        is_rate.dislike = 0
        db.session.commit()

    return redirect(
        url_for('main.show_post', id=id)
    )


@bp.route('/dislike/set/<id>', methods=['GET', 'POST'])
@login_required
def set_dislike(id):
    post = Post.query.filter_by(id=id).first()
    is_rate = Rate.query.filter_by(user_id=current_user.id).filter_by(post_id=post.id).first()
    if not is_rate:
        rate = Rate(user_id=current_user.id, post_id=post.id, like=1, dislike=0)
        db.session.add(rate)
        db.session.commit()
        return redirect(
            url_for('main.show_post', id=id)
        )
    elif is_rate.dislike == 0 and is_rate.user_id == current_user.id and is_rate.post_id == post.id:
        is_rate.dislike = 1
        is_rate.like = 0
        db.session.commit()

    return redirect(
        url_for('main.show_post', id=id)
    )


@bp.route('/files/<filename>')
def uploaded_files(filename):
    path = current_app.config['UPLOADED_PATH']
    return send_from_directory(path, filename)


def allowed_extension(extension):
    if extension in ['.gif', '.jpg', '.jpeg', '.png', '.pdf']:
        return True
    return False

def gen_rnd_filename():
    filename_prefix = datetime.now().strftime('%Y%m%d%H%M%S')
    return '%s%s' % (filename_prefix, str(random.randrange(1000, 10000)))


@bp.route('/ckupload/', methods=['POST', 'OPTIONS'])
def ckupload():
    """CKEditor file upload"""
    error = ''
    url = ''
    callback = request.args.get("CKEditorFuncNum")
    if request.method == 'POST' and 'upload' in request.files:
        fileobj = request.files['upload']
        fname, fext = os.path.splitext(fileobj.filename)
        if not allowed_extension(fext):
            return upload_fail(message=_l('Only images and pdf files are allowed.'))
        rnd_name = '%s%s' % (gen_rnd_filename(), fext)
        fileobj.filename = rnd_name
        filepath = os.path.join(current_app.static_folder, 'upload', rnd_name)
        dirname = os.path.dirname(filepath)
        if not os.path.exists(dirname):
            try:
                os.makedirs(dirname)
            except:
                error = 'ERROR_CREATE_DIR'
        elif not os.access(dirname, os.W_OK):
            error = 'ERROR_DIR_NOT_WRITEABLE'
        if not error:
            fileobj.save(filepath)
            url = url_for('static', filename='%s/%s' % ('upload', rnd_name))
    else:
        error = 'post error'
    res = """<script type="text/javascript">
             window.parent.CKEDITOR.tools.callFunction(%s, '%s', '%s');
             </script>""" % (callback, url, error)
    response = make_response(res)
    response.headers["Content-Type"] = "text/html"
    url = url_for('main.uploaded_files', filename=fileobj.filename)
    return upload_success(url)

@bp.route('/category/create', methods=['GET', 'POST'])
@login_required
def create_category():
    form = CategoryForm()
    if form.validate_on_submit():
        category = Category(title=form.title.data, description=form.description.data)
        db.session.add(category)
        db.session.commit()
        flash(_l('Your category is now created!'))
        return redirect(url_for('main.show_category', id=category.id))
    return render_template(
        'main/create_category.html.j2',
        title=_l('Creating Post'),
        form=form
    )

@bp.route('/category/edit/<id>', methods=['GET', 'POST'])
@login_required
def edit_category(id):
    category = Category.query.filter_by(id=id).first()
    form = EditCategoryForm()
    if form.validate_on_submit():
        category.title = form.title.data
        category.description = form.description.data
        db.session.commit()
        flash(_l('Your changes have been saved'))
        return redirect(url_for('main.show_category', id=category.id))
    elif request.method == 'GET':
        form.title.data = category.title
        form.description.data = category.description
    return render_template('main/edit_category.html.j2', form=form)

@bp.route('/category/show/<id>', methods=['GET'])
@login_required
def show_category(id):
    category = Category.query.filter_by(id=id).first()
    posts = Post.query.filter_by(category_id=id).all()
    categories = Category.query.all()

    return render_template(
        'main/single_category.html.j2',
        category=category,
        posts=posts,
        categories=categories
    )

@bp.route('/category/delete/<id>')
@login_required
def delete_category(id):
    posts = Post.query.filter_by(category_id=id).all()
    category = Category.query.filter_by(id=id).first()

    if posts:
        flash(_l('First delete the articles connected with this category.'))
        redirect(url_for('main.show_category', id=id))
    else:
        try:
            db.session.delete(category)
            db.session.commit()
        except:
            return redirect(url_for('main.index'))

    return redirect(url_for('main.index'))
